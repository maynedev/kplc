<?php

namespace App\Http\Controllers;

use App\System;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SystemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getClient(Request $request)
    {
        $bearerToken = $request->bearerToken();
        $tokenId = (new \Lcobucci\JWT\Parser())->parse($bearerToken)->getHeader('jti');
        $client = \Laravel\Passport\Token::find($tokenId)->client;

        $client_id = $client->id;
        $client_secret = $client->secret;

        dd($client);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        $passportId = 3;

        //Generate passport client
        $data = [
            'name' => 'Client Name',
            'redirect' => 'http://example.com/callback'
        ];
        $client = new Client();
        try {
            $res = $client->post(url('api/v1/oauth/clients'), [
                'form_params' => $data
            ]);
            return $res->getBody();
        } catch (\Exception $e) {
            return $e->getMessage();
        }


        System::updateOrCreate([
            'name' => $request->name,
            'passport_client_id' => $passportId
        ]);
        DB::commit();
    }
}
