<?php

namespace App\Http\Controllers;

use App\Message;
use App\System;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Laravel\Passport\Token;
use Lcobucci\JWT\Parser;

class MessageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    private function getClient(Request $request)
    {
        $bearerToken = $request->bearerToken();
        $tokenId = (new Parser())->parse($bearerToken)->getHeader('jti');
        $client = Token::find($tokenId)->client;

        return $client;
    }

    public function sendBulk(Request $request)
    {
        Log::info("Bulk request:", $request);

        foreach ($request->body as $message) {
            //Verify shortcode


            Message::create([
                'short_code' => $message->short_code,
                'message' => $message->message,
                'system_id' => $this->getClient($request)->id,
                'recipients' => $request->recipients,
                'processed_at' => null
            ]);
        }
    }

    public function sendBulkBatch(Request $request)
    {
        DB::beginTransaction();
        $passportId = 3;

        System::updateOrCreate([
            'name' => $request->name,
            'passport_client_id' => $passportId
        ]);
        DB::commit();
    }
}
