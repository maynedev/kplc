<?php


namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MessageRecipient extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'message_id',
        'phone_number',
        'sent_at',
        'delivered_at'
    ];

    public $timestamps = ['delivered_at', 'sent_at'];

    public function message()
    {
        return $this->belongsTo(Message::class, 'message_id');
    }
}
