<?php


namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SystemCode extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'id', 'system_id', 'short_code'
    ];

    public function shortCodes(){

    }
}
