<?php


namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes;


    protected $fillable = [
        'id',
        'system_short_code_id',
        'message',
        'status',
        'processed_at'
    ];

    public function systemShortCode()
    {
        return $this->belongsTo(SystemCode::class, 'system_short_code_id');
    }
}
