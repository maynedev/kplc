<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->group(['middleware' => 'client'], function () use ($router) {
    $router->get('/', 'SystemController@getClient');
    $router->post('/clients', 'SystemController@store');
    $router->post('/send', 'MessageController@sendBulk');
    $router->post('/send/batch', 'MessageController@sendBulkBatch');
});
